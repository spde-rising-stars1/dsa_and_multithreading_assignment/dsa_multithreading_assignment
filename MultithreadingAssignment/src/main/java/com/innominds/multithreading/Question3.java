package com.innominds.multithreading;

class Threads extends Thread {
	public static int flag;
	
    @Override
	public void run() {
	
			if (Thread.currentThread().getPriority() == 8 || Thread.currentThread().getPriority() == 9) {

				if (Thread.currentThread().getPriority() == 9) {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				System.out.println("Currently sleeping thread is : "+Thread.currentThread().getPriority());
			}
				System.out.println("Inside run method of Thread class " + Thread.currentThread().getPriority());
				
		flag = Thread.currentThread().getPriority();
	}
}

public class Question3 {
	public static void createThreads() {
		Threads t1 = new Threads();
		Threads t2 = new Threads();
		Threads t3 = new Threads();
		Threads t4 = new Threads();
		Threads t5 = new Threads();

		t1.setPriority(5);
		t2.setPriority(6);
		t3.setPriority(7);
		t4.setPriority(8);
		t5.setPriority(9);

		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
	}

	public static void main(String[] args) throws Exception {
		createThreads();
		Thread.sleep(3000);
		System.out.println("");
		System.out.println("Long lasting Thread is : " + Threads.flag);
	}
}
