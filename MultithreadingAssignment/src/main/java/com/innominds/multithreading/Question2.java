package com.innominds.multithreading;

public class Question2 implements Runnable {

	public int maximumNumber = 10;
	static int number =1;
	int remainder;
	static Object lock = new Object();

	Question2(int remainder) {
		this.remainder = remainder;
	}

	@Override
	public void run() {
		while (number < maximumNumber) {
			synchronized (lock) {
				while (number % 2 != remainder) {
					try {
						lock.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				System.out.println(Thread.currentThread().getName() + " " + number);
				number++;
				lock.notifyAll();
			}
		}
	}

	public static void main(String[] args) {

		Question2 odd = new Question2(1);
		Question2 even = new Question2(0);

		Thread t1 = new Thread(odd, " Ping ");
		Thread t2 = new Thread(even, " Pong ");

		t1.start();
		t2.start();

	}
}
