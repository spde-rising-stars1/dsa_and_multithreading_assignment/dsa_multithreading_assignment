package com.innominds.multithreading;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

@SuppressWarnings("serial")
public class Question6 implements Serializable {
	String firstName;
	String secondName;
	transient String fullName;
	String email;
	String password;

	public Question6(String firstName, String secondName, String fullName, String email, String password) {
		super();
		this.firstName = firstName;
		this.secondName = secondName;
		this.fullName = fullName;
		this.email = email;
		this.password = password;
	}

	private static void serialize(Question6 employee) {
		try {
			System.out.println("Employee serializing : " + employee.toString());
			FileOutputStream fileOut = new FileOutputStream("employee.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(employee);
			out.close();
			fileOut.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

	private static void deserialize() {
		try {
			FileInputStream fileIn = new FileInputStream("employee.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			Question6 employee = (Question6) in.readObject();
			in.close();
			fileIn.close();
			System.out.println("Employee deserialized : " + employee.toString());
		} catch (IOException | ClassNotFoundException i) {
			i.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return "Question6{" + "firstName='" + firstName + '\'' + ", secondName='" + secondName + '\'' + ", fullName='"
				+ fullName + '\'' + ", email='" + email + '\'' + ", password='" + password + '\'' + '}';
	}

	public static void main(String[] args) {
		Question6 employee = new Question6("Akshay", "Kanse", "Akshay Kanse", "akshay@test.com", "123456");
		serialize(employee);
		deserialize();

	}
}
