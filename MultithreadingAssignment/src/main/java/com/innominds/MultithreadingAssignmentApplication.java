package com.innominds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultithreadingAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultithreadingAssignmentApplication.class, args);
	}

}
