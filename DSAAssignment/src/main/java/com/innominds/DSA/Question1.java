package com.innominds.DSA;

import java.util.ArrayList;

public class Question1 {

	private static ArrayList<Integer> A1 = new ArrayList<Integer>();

	public static ArrayList<Integer> saveEvenNumbers(int N) {
		A1 = new ArrayList<Integer>();

		for (int i = 2; i <= N; i++) {
			if (i % 2 == 0)
				A1.add(i);
		}
		System.out.println("Elements of ArrayList are A1 " + A1);
		return A1;
	}

	public static ArrayList<Integer> printEvenNumbers() {
		ArrayList<Integer> A2 = new ArrayList<Integer>();
		for (int list : A1) {
			A2.add(list * 2);
		}
		System.out.println("Elements of ArrayList are A2 " + A2);
		return A2;
	}

	public static int printEvenNumbers(int N) {
		for (int list : A1) {
			if (list == N) {
				System.out.println("The number " +list+ " is present is the ArrayList. ");
			}	
		}
		return 0;
	}

	public static void main(String[] args) {
		Question1.saveEvenNumbers(20);
		Question1.printEvenNumbers();
		Question1.printEvenNumbers(18);

	}
}
