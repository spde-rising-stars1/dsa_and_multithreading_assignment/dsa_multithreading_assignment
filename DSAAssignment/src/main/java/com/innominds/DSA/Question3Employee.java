package com.innominds.DSA;

import java.util.Objects;

public class Question3Employee {
	private int EmpId;
	private String EmpName;
	private String EmpEmail;
	private char EmpGender;
	private float EmpSalary;

	public Question3Employee() {
	}

	public Question3Employee(int empId, String empName, String empEmail, char empGender, float empSalary) {
		super();
		EmpId = empId;
		EmpName = empName;
		EmpEmail = empEmail;
		EmpGender = empGender;
		EmpSalary = empSalary;
	}

	public String GetEmployeeDetails() {
		return "Employee [EmpId=" + EmpId + ", EmpName=" + EmpName + ", EmpEmail=" + EmpEmail + ", EmpGender="
				+ EmpGender + ", EmpSalary=" + EmpSalary + "]";
	}

	public int getEmpId() {
		return EmpId;
	}

	public void setEmpId(int empId) {
		EmpId = empId;
	}

	public String getEmpName() {
		return EmpName;
	}

	public void setEmpName(String empName) {
		EmpName = empName;
	}

	public String getEmpEmail() {
		return EmpEmail;
	}

	public void setEmpEmail(String empEmail) {
		EmpEmail = empEmail;
	}

	public char getEmpGender() {
		return EmpGender;
	}

	public void setEmpGender(char empGender) {
		EmpGender = empGender;
	}

	public float getEmpSalary() {
		return EmpSalary;
	}

	public void setEmpSalary(float empSalary) {
		EmpSalary = empSalary;
	}

	@Override
	public int hashCode() {
		return Objects.hash(EmpEmail, EmpGender, EmpId, EmpName, EmpSalary);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Question3Employee other = (Question3Employee) obj;
		return Objects.equals(EmpEmail, other.EmpEmail) && EmpGender == other.EmpGender && EmpId == other.EmpId
				&& Objects.equals(EmpName, other.EmpName)
				&& Float.floatToIntBits(EmpSalary) == Float.floatToIntBits(other.EmpSalary);
	}

}