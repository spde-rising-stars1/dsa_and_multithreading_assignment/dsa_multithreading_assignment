package com.innominds.DSA;

public class Question3EmployeeMain {

	public static void main(String[] args) {

		Question3Employee employee1 = new Question3Employee(1, "Adesh Kesarkar", "adesh@test.com", 'M', 40000);
		Question3Employee employee2 = new Question3Employee(2, "Vaibhav Sahu", "vaibhav@test.com", 'M', 45000);
		Question3Employee employee3 = new Question3Employee(3, "Kunal Asnani", "kunal@test.com", 'M', 50000);
		Question3Employee employee4 = new Question3Employee(4, "Amit Anwade", "amit@test.com", 'M', 55000);

		Question3EmployeeDB.addEmployee(employee1);
		Question3EmployeeDB.addEmployee(employee2);
		Question3EmployeeDB.addEmployee(employee3);
		Question3EmployeeDB.addEmployee(employee4);

		for (Question3Employee employee : Question3EmployeeDB.listAll())
			System.out.println(employee.GetEmployeeDetails());

		System.out.println();
		Question3EmployeeDB.deleteEmployee(2);

		for (Question3Employee employee : Question3EmployeeDB.listAll())
			System.out.println(employee.GetEmployeeDetails());

		System.out.println();

		System.out.println(Question3EmployeeDB.showPaySlip(3));
	}

}
