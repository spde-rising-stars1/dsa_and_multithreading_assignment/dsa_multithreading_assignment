package com.innominds.DSA;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Question3EmployeeDB {
	static List<Question3Employee> employeedbarraylist = new ArrayList<>();

	public static boolean addEmployee(Question3Employee e) {
		return employeedbarraylist.add(e);
	}

	public static boolean deleteEmployee(int empId) {
		boolean removed = false;
		Iterator<Question3Employee> iterator = employeedbarraylist.iterator();

		while (iterator.hasNext()) {
			Question3Employee employee = iterator.next();
			if (employee.getEmpId() == empId) {
				removed = true;
				iterator.remove();
			}
		}
		return removed;
	}

	public static String showPaySlip(int empId) {
		String payslip = "Invalid employeeId";

		for (Question3Employee e : employeedbarraylist) {
			if (e.getEmpId() == empId) {
				payslip = "Pay slip for employeeId " + empId + " is " + e.getEmpSalary();
			}
		}

		return payslip;
	}

	public static Question3Employee[] listAll() {
		Question3Employee[] employeearray = new Question3Employee[employeedbarraylist.size()];
		for (int i = 0; i < employeedbarraylist.size(); i++)
			employeearray[i] = employeedbarraylist.get(i);
		return employeearray;
	}

}