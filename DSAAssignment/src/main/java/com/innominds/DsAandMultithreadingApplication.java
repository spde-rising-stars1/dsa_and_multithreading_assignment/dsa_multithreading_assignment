package com.innominds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DsAandMultithreadingApplication {

	public static void main(String[] args) {
		SpringApplication.run(DsAandMultithreadingApplication.class, args);
	}

}
